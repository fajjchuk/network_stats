import styled from "styled-components";
import { Row, DropdownButton } from "react-bootstrap";

const spacing = {
  small: '16px',
  medium: '32px',
  large: '48px',
};

export const NavbarHeading = styled.h1`
  color: #fff;
  display: flex;
  width: 100%;
  justify-content: center;
  margin: 0;
`;

export const Heading = styled(Row)`
  padding-top: ${spacing.medium};
  padding-bottom: ${spacing.small};
`;

export const ChartContainer = styled(Row)`
  height: 400px;
`;

export const StyledDropdown = styled(DropdownButton)`
  justify-content: flex-end;
  display: flex;
`;

export const PageWrapper = styled.div`
  padding-top: 64px;
`;

export const TooltipContent = styled.div`
  box-shadow: 5px 3px 30px black;
  overflow: hidden;
  background: hsla(0,0%,100%,.5);
  border-radius: 10px;
  padding: 10px;
`;