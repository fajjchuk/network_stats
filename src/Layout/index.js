import React, {useState} from "react";
import { 
  Col,
  Nav,
  Navbar, 
  Container, 
  NavDropdown,
  Button,
  Table,
  Row,
} from 'react-bootstrap';
import {
  AreaChart, 
  Area, 
  XAxis, 
  YAxis, 
  CartesianGrid, 
  Tooltip,
  ResponsiveContainer,
} from 'recharts';
import { head, equals, not } from 'ramda';
import { isAfter, isBefore } from 'date-fns';
import { 
  Heading,
  ChartContainer,
  PageWrapper,
  TooltipContent,
} from './styles';

import measures from '../measures.json';

const formatBytes = (bytes, decimals = 2) => {
  if (bytes === 0) return '0 Bytes';
  const k = 1024;
  const dm = decimals < 0 ? 0 : decimals;
  const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

  const i = Math.floor(Math.log(bytes) / Math.log(k));

  return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
}

const getChartData = ({measures: { data }, startDate, endDate}) => {
  if (endDate) {
    return data.filter(({date}, i) => 
      isAfter(new Date(date), new Date(startDate)) && isBefore( new Date(date), new Date(endDate))
      || equals(startDate, date)
      || equals(endDate, date)
    )
  }
  return data.filter(({date}, i) => equals(startDate, date));
};

const dates = [
  "04-27-2020",
  "04-28-2020",
  "04-29-2020",
  "04-30-2020",
  "05-01-2020",
  "05-02-2020",
  "05-03-2020",
];

const isEndDateItemDisabled = (date, startDate) => not(
  isAfter(
    new Date(date),
    new Date(startDate),
  )
);

const isStartDateItemDisabled = (date, endDate) => endDate && not(
  isBefore(
    new Date(date),
    new Date(endDate),
  )
);

const isEndDateItemActive = (date, endDate) => endDate && equals(date, endDate);

const CustomTooltip = ({ active, payload, label }) => {
  if (active) {
    const { payload: {date}, value } = head(payload);
    return (
      <TooltipContent>
        <div>{`Time: ${label}`}</div>
        <div>{`Date: ${date}`}</div>
        <div>{`Received: ${formatBytes(value)}`}</div>
      </TooltipContent>
    );
  }
  return null;
};

const getTotalReceivedData = (chartData = []) => chartData.reduce((acc, {rx_bytes}) => acc + rx_bytes, 0);

const Layout = () => {
  const [ startDate, setStartDate ] = useState(head(dates));
  const [ endDate, setEndDate ] = useState(null);

  const resetFilter = () => {
    setStartDate(head(dates));
    setEndDate(null);
  };

  const chartData = getChartData({
    measures,
    startDate,
    endDate,
  }); 

  const total = getTotalReceivedData(chartData); 

  return (
    <PageWrapper>
      <Navbar bg="primary" variant="dark" fixed="top">
        <Navbar.Brand>Network statistic</Navbar.Brand>
        <Navbar.Collapse className="justify-content-end">
          <Nav>
            <NavDropdown title={`Start Date: ${startDate}`}>
              {dates.map((date, i) => (
                <NavDropdown.Item 
                  key={date}
                  disabled={isStartDateItemDisabled(date, endDate)}
                  active={equals(date, startDate)}
                  eventKey={date}
                  onSelect={setStartDate}
                >
                  {date}
                </NavDropdown.Item>
              ))}
            </NavDropdown>
            <NavDropdown title={endDate ? `End Date: ${endDate}`: 'Select End Date'}>
              {dates.map((date, i) => (
                <NavDropdown.Item 
                  key={date}
                  disabled={isEndDateItemDisabled(date, startDate)}
                  active={isEndDateItemActive(date, endDate)}
                  eventKey={date}
                  onSelect={setEndDate}
                >
                  {date}
                </NavDropdown.Item>
              ))}
            </NavDropdown>
            <Button variant="success" onClick={resetFilter}>Reset filter</Button>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
      <Container>
      <Heading>
          <Col>
            <h3>
              Received Data
            </h3>
          </Col>
        </Heading>
        <ChartContainer>
          <Col>
            <ResponsiveContainer>
              <AreaChart data={chartData}>
                <CartesianGrid strokeDasharray="3 3" />
                <XAxis dataKey="hour" name="Date"/>
                <YAxis />
                <Tooltip content={<CustomTooltip />}/>
                <Area 
                  type="monotone" 
                  dataKey="rx_bytes" 
                  name="Received KB" 
                  stroke="#8884d8" 
                  fill="#8884d8" 
                />
              </AreaChart>
            </ResponsiveContainer>
          </Col>
        </ChartContainer>
        <hr/>
        <Row>
          <Col md={{ span: 6, offset: 6 }}>
            <Table striped bordered hover size="sm">
              <tbody>
                <tr>
                  <td>Total received data</td>
                  <td>{formatBytes(total)}</td>
                </tr>
                <tr>
                  <td>{endDate ? 'Start date' : 'Date'}</td>
                  <td>{startDate}</td>
                </tr>
                {endDate && (
                  <tr>
                    <td>End date</td>
                    <td>{endDate}</td>
                  </tr>
                )}
              </tbody>
            </Table>
          </Col>
        </Row>
      </Container>
    </PageWrapper>
  );
}

export default Layout;

